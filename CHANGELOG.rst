=========
Changelog
=========


5.0.7.0 (2024-07-18)
====================

* Upgrade Django to 5.0.7
  see https://www.djangoproject.com/weblog/2024/jul/09/security-releases/ for details


5.0.6.0 (2024-05-22)
====================

* Added support for Django 5.0.6


5.0.3.0 (2024-03-04)
====================

* Upgrade Django to 5.0.3 (fixes CVE-2024-27351)
  see https://www.djangoproject.com/weblog/2024/mar/04/security-releases/ for details


5.0.2.0 (2024-02-07)
====================

* Upgrade Django to 5.0.2 (fixes CVE-2024-24680)
  see https://www.djangoproject.com/weblog/2024/feb/06/security-releases/ for details


5.0.1.0 (2024-01-03)
====================

* Added support for Django 5.0.1
